using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickenSoundsManager : MonoBehaviour
{
    public int maxAudioSources = 5;
    public int minAudioSources = 2;
    public float delayBetweenStarts = .5f;

    public List<AudioClip> chickenSounds;

    private List<AudioSource> mAudioSources = new List<AudioSource>();
    private List<Coroutine> mCoroutines = new List<Coroutine>();
    private int maxChickenNum;
    public int MaxChickenNum
    {
        set { maxChickenNum = value; }
    }

    void Start()
    {
        for (int i = 0; i < maxAudioSources; i++)
        {
            AudioSource audioSource = gameObject.AddComponent<AudioSource>();
            mAudioSources.Add(audioSource);
            mCoroutines.Add(StartCoroutine(MakeSound(audioSource, delayBetweenStarts * i)));

        }
    }

    void Update()
    {
        while (SourceNumberToPlay() < mCoroutines.Count)
        {
            StopCoroutine(mCoroutines[0]);
            mCoroutines.RemoveAt(0);
            Destroy(mAudioSources[0]);
            mAudioSources.RemoveAt(0);
        }
    }

    // The number of AudioSources to use relative on the number of chickens still alive
    private int SourceNumberToPlay()
    {
        return (int) Mathf.Round(Remap(1, maxChickenNum, minAudioSources, maxAudioSources, GameManager.instance.galline.Count));
    }

    private IEnumerator MakeSound(AudioSource audioSource, float delay)
    {
        yield return new WaitForSeconds(delay);

        while (true)
        {
            int audioClipIndex = Random.Range(0, chickenSounds.Count - 1);
            AudioClip audioClip = chickenSounds[audioClipIndex];
            audioSource.clip = audioClip;
            audioSource.Play();

            yield return new WaitForSeconds(audioClip.length + Random.Range(3f, 6f));
        }
    }

    private float Remap(float iMin, float iMax, float oMin, float oMax, float v)
    {
        float t = Mathf.InverseLerp(iMin, iMax, v);
        return Mathf.Lerp(oMin, oMax, t);
    }
}
