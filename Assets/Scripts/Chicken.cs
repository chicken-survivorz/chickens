using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public enum ChickenMovementState
{
    IDLE,
    SCARED,
    PERMA_STUNNED,
    STUNNED,
    VICTORY_SPRINT
}

public class Chicken : MonoBehaviour
{
    [Header("References")]
    public SkinnedMeshRenderer smRenderer;
    public Material idleMat;
    public Material scaredMat;
    public GameObject chickenGraphics;
    public ParticleSystem featherParticles;
    public GameObject stunnedParticles;
    
    private Animator animator;


    [Header("Idle")]
    public float nextSprintMinTimeIdle = 1f;
    public float nextSprintMaxTimeIdle = 5f;
    public float sprintMinDuration = 1f;
    public float sprintMaxDuration = 3f;
    [SerializeField] private float _movementSpeedIdle = 1f;
    public float movementSpeedIdle
    {
        get => _movementSpeedIdle * speedMultiplier;
        set => _movementSpeedIdle = value;
    }
    
    public float forcedOffscreenIdleTime = 0.5f;

    private float stoppingTime = 0f;

    [Header("Scared")]
    public float nextSprintMinTimeScared = 0.5f;
    public float nextSprintMaxTimeScared = 1f;
    [SerializeField] private float _movementSpeedScared = 6f;
    public float movementSpeedScared
    {
        get => _movementSpeedScared * speedMultiplier;
        set => _movementSpeedScared = value;
    }
    public float movementDelta = 20f;

    [Header("Other Properties")]
    public float pushForce = 2f;
    public float accellerationTimer = 0.1f;
    public float viewRange = 5f;
    public float runningAnimationMaxDelay = 0.5f;
    public float screenEndYBuffer = 30f;
    public float speedMultiplier = 1f;
    public ChickenMovementState state = ChickenMovementState.IDLE;

    // Private members
    private Vector3 targetSpeed = Vector3.zero;
    private float nextSprintTime = 0;

    private float forcedIdleTimer;

    private Vector3 lastVelocity;
    private float currentAccellerationTimer;

    private bool imDying = false;

    private Rigidbody body;

    public ChickenCoopSaver AssignedCoop { get; set; }




    private AudioSource audioSource;

    private GameObject particlesStatus;
    private IStatus _status;
    public IStatus Status
    {
        get => _status;
        set
        {
            if (value == null)
            {
                smRenderer.material = idleMat;
                _status = null;
            }
            else if(!value.GetStatusTag().Equals(_status?.GetStatusTag()))
                _status = value;
        }
    }


    private bool isCoroutineRunning = false;

    void Start()
    {
        animator = chickenGraphics.GetComponent<Animator>();
        body = GetComponent<Rigidbody>();

        audioSource = GetComponent<AudioSource>();

    }

    void Update()
    {
        /* to refactor this Update,
         * states should be a hierarchy of classes
         * with polyphormic behavioural method instead of switch(state){...
         */



        if (state != ChickenMovementState.PERMA_STUNNED && Time.time > forcedIdleTimer)
        {
            if (EnemyManager.instance.IsEnemyInRange(transform.position, viewRange))
            {
                ChangeState(ChickenMovementState.SCARED);
            }
            else
            {
                ChangeState(ChickenMovementState.IDLE);
            }
        }


        if (IsAboveScreen())
        {
            ChangeState(ChickenMovementState.IDLE);
            forcedIdleTimer = Time.time + forcedOffscreenIdleTime;
        }

        if (AssignedCoop != null) state = ChickenMovementState.VICTORY_SPRINT;

        switch (state)
        {
            case ChickenMovementState.IDLE:
                NextIdleSprint();
                break;
            case ChickenMovementState.SCARED:
                NextScaredSprint();
                break;
            case ChickenMovementState.PERMA_STUNNED:
            case ChickenMovementState.STUNNED:
                targetSpeed = Vector3.zero;
                break;
            case ChickenMovementState.VICTORY_SPRINT:
                NextVictorySprint();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        Status?.Update();
    }
    bool IsAboveScreen()
    {
        if (Camera.main.WorldToScreenPoint(transform.position).y >= Screen.height - screenEndYBuffer) return true;
        return false;
    }

    void NextIdleSprint()
    {
        if (Time.time >= nextSprintTime)
        {
            animator.SetBool("Walking", true);

            nextSprintTime = Time.time + Random.Range(nextSprintMinTimeIdle, nextSprintMaxTimeIdle);

            // completely random
            Vector3 newDirection = new Vector3(Random.Range(-1f, 1f), 0f, Random.Range(-1f, 1f)).normalized;
            transform.LookAt(transform.position + newDirection);
            targetSpeed = newDirection * movementSpeedIdle;
            stoppingTime = Time.time + Random.Range(sprintMinDuration, sprintMaxDuration);
        }
        else if (Time.time >= stoppingTime)
        {
            targetSpeed = Vector3.zero;
        }
    }

    void NextScaredSprint()
    {
        if (Time.time >= nextSprintTime)
        {
            if (!isCoroutineRunning) StartCoroutine(SetupRunning());

            nextSprintTime = Time.time + Random.Range(nextSprintMinTimeScared, nextSprintMaxTimeScared);

            Vector3 newDirection = EnemyManager.instance.GetNewDirection(transform.position, viewRange, 
                _status?.GetStatusTag() == StatusEffectTag.Poison);

            // slight random offset
            newDirection = Quaternion.Euler(0, Random.Range(-movementDelta, movementDelta), 0) * newDirection;

            if(Time.timeScale != 0)
                transform.LookAt(transform.position + newDirection);

            targetSpeed = newDirection * movementSpeedScared;
        }
    }

    void NextVictorySprint()
    {
        animator.SetBool("Running", true);
        Vector3 newDirection = AssignedCoop.transform.position - this.transform.position;
        newDirection.y = 0;
        newDirection.Normalize();
        if(Time.timeScale != 0)
            transform.LookAt(transform.position + newDirection);
        targetSpeed = newDirection * movementSpeedScared;
    }

    IEnumerator SetupRunning()
    {
        if (isCoroutineRunning) yield return null;
        isCoroutineRunning = true;
        yield return new WaitForSeconds(Random.Range(0f, runningAnimationMaxDelay));
        isCoroutineRunning = false;
        animator.SetBool("Running", true);
        yield return null;
    }

    public void Die()
    {
        if(!imDying)
            Die(.5f);
    }

    public void Die(float timeToDie)
    {
        if (!imDying)
        {
            imDying = true;
            chickenGraphics.SetActive(false);
            featherParticles.gameObject.SetActive(true);
            _status = null;

            featherParticles.Play();

            GameManager.instance.OnGallinaDeath(this.gameObject);
            Destroy(this.gameObject, timeToDie);
        }
    }

    void FixedUpdate()
    {
        if (targetSpeed != Vector3.zero)
        {
            if (lastVelocity != targetSpeed)
            {
                currentAccellerationTimer = 0;
            }
            else 
                currentAccellerationTimer+= + Time.fixedDeltaTime;
            body.velocity = Vector3.Lerp(body.velocity, targetSpeed, (currentAccellerationTimer/(accellerationTimer)));
            lastVelocity = targetSpeed;
        }
        else
        {
            animator.SetBool("Walking", false);
            animator.SetBool("Running", false);
        }
        body.velocity += Vector3.up * -9.81f;
    }

    private void ChangeState(ChickenMovementState newState)
    {
        if (state == ChickenMovementState.IDLE && newState == ChickenMovementState.SCARED)
        {
            nextSprintTime = Time.time;
            state = newState;
            //smRenderer.material = scaredMat;
        }
        else if (state == ChickenMovementState.SCARED && newState == ChickenMovementState.IDLE)
        {
            state = newState;
            stoppingTime = 0;
            //smRenderer.material = idleMat;
        }
    }

    public void RemoveParticles()
    {
        if (particlesStatus)
        {
            Destroy(particlesStatus);
            particlesStatus = null;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (Status is IStatusWithPropagation)
        {
            ((IStatusWithPropagation)Status)?.CollisionEnter(collision);
        }
        if (collision.gameObject.CompareTag("Gallina"))
        {
            var random = Random.insideUnitSphere;
            random.y = 0;
            random.Normalize();
            collision.gameObject.GetComponent<Rigidbody>().AddForce(random * pushForce, ForceMode.Impulse);
        }

    }

    private void OnCollisionStay(Collision collision)
    {
        if (Status is IStatusWithPropagation)
        {
            ((IStatusWithPropagation)Status)?.CollisionStay(collision);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (Status is IStatusWithPropagation)
        {
            ((IStatusWithPropagation)Status)?.CollisionExit(collision);
        }
    }

    public void SetParticleStatus(GameObject particles)
    {
        Destroy(particlesStatus);
        if(particles)
        {
            particlesStatus = Instantiate(particles, transform);
            particlesStatus.GetComponent<ParticleSystem>().Play();
        }
    }
}
