using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public class MakeGallineToBeInFuga : MonoBehaviour
{
    
    [SerializeField]
    private float speed = 1.0f;

    private float originalSpeed;

    public PathCreator pathCreator;

    float pos = 0;

    public float StoppingTime = 1f;

    private bool levelEnded = false;

    void Start()
    {
        originalSpeed = speed;
    }

    void Update()
    {
        if (pathCreator != null)
        {
            pos += Time.deltaTime * speed;
            if (!levelEnded)
            {
                transform.position = pathCreator.path.GetPointAtDistance(pos);
                transform.rotation = pathCreator.path.GetRotationAtDistance(pos);
                //pathCreator.path.length 

                GameManager.instance.UIManager.SetProgressValue(pos);
            }
            else
            {
                speed = Mathf.MoveTowards(speed, 0, originalSpeed * (StoppingTime * Time.deltaTime));
                transform.position += transform.forward * (speed * Time.deltaTime);
            }
        }
        else
        {

        }
    }

    public void StopMoving()
    {
        levelEnded = true;
    }
}
