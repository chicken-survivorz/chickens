using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMainPericolo : MonoBehaviour
{

    [SerializeField]
    Transform mainPericolo;

    [SerializeField] private AnimationCurve victoryPan;

    private bool gameOverWithVictory;
    private float curveTimer = 0f;

    float offset;
    private float panOffset = 0f;

    private void Start()
    {
        offset = transform.position.z - mainPericolo.position.z;
    }

    void LateUpdate()
    {
        if (gameOverWithVictory)
        {
            float totalLength = victoryPan.keys[victoryPan.length - 1].time;
            if (curveTimer < totalLength)
            {
                curveTimer += Time.deltaTime;
                if (curveTimer > totalLength)
                {
                    curveTimer = totalLength;
                }

                panOffset = victoryPan.Evaluate(curveTimer);
            }
        }
        transform.position = new Vector3(transform.position.x,transform.position.y, mainPericolo.position.z + offset + panOffset);
        transform.rotation = Quaternion.Euler(transform.eulerAngles.x, mainPericolo.eulerAngles.y, transform.eulerAngles.z);

    }

    public void StartVictoryPan()
    {
        gameOverWithVictory = true;
    }
}
