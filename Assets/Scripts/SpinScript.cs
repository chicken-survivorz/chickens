using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinScript : MonoBehaviour
{
    public Vector3 axis;

    public float speed;
    private float originalSpeed;


    public float StoppingTime = 1f;
    private bool levelEnded = false;

    private void Start()
    {
        originalSpeed = speed;
    }

    private void Update()
    {
        if (levelEnded)
        {
            speed = Mathf.MoveTowards(speed, 0, originalSpeed * (StoppingTime * Time.deltaTime));
        }
        transform.Rotate(axis, speed * Time.deltaTime);
    }

    public void StopSpinning()
    {
        levelEnded = true;
    }
}
