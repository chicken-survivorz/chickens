using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateWolf : MonoBehaviour
{
    public Wolf wolf;

    private void OnTriggerEnter(Collider other)
    {
        Chicken chicken = other.gameObject.GetComponent<Chicken>();
        if (chicken)
        {
            wolf.gameObject.SetActive(true);
            Destroy(gameObject);
        }
    }
}
