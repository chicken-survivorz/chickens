using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public class FencePath : MonoBehaviour
{

    public GameObject[] fences;
    public GameObject[] gates;
    public GameObject[] shrubs;
    public PathCreator pathCreator;
    public VertexPath vertexPath;
    public float spawnRotationOffset = 90f;

    public float gateChance;
    public float shrubChance;
    public int gateCooldown = 5;
    public float shrubOffset;

    public bool isLeftSide;


    public FencePath otherFencePath;
    public int chosenFenceIndex = -1;
    
    
    
    float prevLength = 0;
    


    void Start() 
    {
        if (otherFencePath == null)
        {
            foreach (var fencePath in FindObjectsOfType<FencePath>())
            {
                if (fencePath != this) otherFencePath = fencePath;
            }
        }
        if (chosenFenceIndex == -1)
        {
            if (otherFencePath != null)
            {
                if (otherFencePath.chosenFenceIndex == -1)
                {
                    otherFencePath.chosenFenceIndex = Random.Range(0, fences.Length);
                }
                chosenFenceIndex = otherFencePath.chosenFenceIndex;
            }
            else
            {
                chosenFenceIndex = Random.Range(0, fences.Length);
            }
        }


        GameObject spawnedPiece;
        Collider collider;
       
        pathCreator = GetComponent<PathCreator>();
        
        if (vertexPath == null)
        {
            vertexPath = pathCreator.path;
        }

        int gateAllowanceReverseCounter = gateCooldown;
        while (prevLength < vertexPath.length)
        {
            Vector3 pos = vertexPath.GetPointAtDistance(prevLength);
            Quaternion rotation = vertexPath.GetRotationAtDistance(prevLength);
            rotation = Quaternion.Euler(rotation.eulerAngles.x, rotation.eulerAngles.y, rotation.eulerAngles.z);
            float choice = Random.Range(0f, 100f);
            if (choice < gateChance && gateAllowanceReverseCounter <= 0)
            {
                int index = Random.Range(0, gates.Length);
                spawnedPiece = Instantiate(gates[index], pos, Quaternion.identity, this.transform);
                gateAllowanceReverseCounter = gateCooldown;
            }
            else
            {
                spawnedPiece = Instantiate(fences[chosenFenceIndex], pos, Quaternion.identity, this.transform);
                if (gateAllowanceReverseCounter > 0) gateAllowanceReverseCounter--;
            }
            collider = spawnedPiece.GetComponentInChildren<Collider>();
            if (collider != null) prevLength += collider.bounds.size.x;
            spawnedPiece.transform.rotation *= rotation * Quaternion.Euler(0, spawnRotationOffset, 0);

            choice = Random.Range(0f, 100f);
            if (choice < shrubChance)
            {
                int index = Random.Range(0, shrubs.Length);
                float offsetJitter = Random.Range(-shrubOffset / 2, shrubOffset / 2);
                Vector3 newPos = rotation * 
                                 ((isLeftSide ? Vector3.left : Vector3.right) * (shrubOffset + offsetJitter)) + 
                                 pos;
                newPos.y = pos.y;

                spawnedPiece = Instantiate(shrubs[index], 
                                            newPos, 
                                            Quaternion.Euler(0f, Random.Range(0f, 360f), 0f), 
                                            this.transform);

            }
        }
    }




}