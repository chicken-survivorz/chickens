using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelButtonLocker : MonoBehaviour
{
    [SerializeField] private int level;
    [SerializeField] private Button button;
    // Start is called before the first frame update
    void Start()
    {
        button.interactable = MainMenuSceneLoader.IsLevelEnabled(level);
    }
}
