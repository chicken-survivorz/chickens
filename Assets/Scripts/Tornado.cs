using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public enum TornadoState
{
    ATTRACTING,
    REPELLING
}

public class Tornado : MonoBehaviour
{
    public Transform chickensRoot;
    public float attractiveForce;
    public float repellingForce;
    public float repellingInitialSpeed;
    public float repellingStateDuration = 5f;
    public float attractionRange = 30f;
    public int capacity = 6;
    public float offsetChickenFromCenterWhenRepelling = .5f;
    public ParticleSystem attractiveParticles;
    public ParticleSystem repellingParticles;
    
    public List<Transform> capturedChickens = new List<Transform>();

    public TornadoState state = TornadoState.ATTRACTING;

    private Timer switchBackToAttractingCountdown;
    
    void Start()
    {
        switchBackToAttractingCountdown = new Timer(repellingStateDuration);
        if (chickensRoot == null)
            chickensRoot = GameObject.Find("Galline").transform;
    }

    void Update()
    {
        if (state == TornadoState.ATTRACTING)
        {
            // attract nearby chickens
            foreach (Transform chickenT in chickensRoot)
            {
                if (Vector3.Distance(transform.position, chickenT.position) < attractionRange)
                {
                    Vector3 force = (transform.position - chickenT.position).normalized * attractiveForce;
                    // NOTE: this assumes chickens have a Rigidbody. This requirement is not coded anywhere
                    // and thus it might break easily
                    chickenT.GetComponent<Rigidbody>().AddForce(force, ForceMode.Force);
                }
            }
        }
        else // state == repelling
        {
            // expel chickens that have been captured for some time
            foreach (Transform chickenT in chickensRoot)
            {
                Vector3 force = (clipToXZ(chickenT.position) - clipToXZ(transform.position)).normalized * repellingForce;
                // NOTE: this assumes chickens have a Rigidbody. This requirement is not coded anywhere
                // and thus it might break easily
                chickenT.GetComponent<Rigidbody>().AddForce(force, ForceMode.Force);
            }

            if (switchBackToAttractingCountdown.HasExpired())
            {
                // switch back to attracting after some time
                state = TornadoState.ATTRACTING;
                attractiveParticles.Play();
            }
        }
    }

    static Vector3 clipToXZ(Vector3 v)
    {
        v.y = 0;
        return v;
    }

    private void OnTriggerEnter(Collider other)
    // private void OnCollisionEnter(Collision other)
    {
        // capture chicken
        var g = other.gameObject.GetComponent<Chicken>();
        if (g && state == TornadoState.ATTRACTING)
        {
            g.GetComponent<Chicken>().state = ChickenMovementState.STUNNED;
            
            capturedChickens.Add(g.transform);

            if (capturedChickens.Count >= capacity)
            {
                // change state
                state = TornadoState.REPELLING;
                attractiveParticles.Stop();
                repellingParticles.Play();
                
                // push all chickens out
                foreach(Transform chickenT in capturedChickens)
                {
                    if (chickenT == null) return;
                    chickenT.GetComponent<Chicken>().state = ChickenMovementState.SCARED;
                    
                    Vector3 randomOffset = (Quaternion.Euler(0, UnityEngine.Random.Range(0, 360f), 0)
                                           * Vector3.forward).normalized * offsetChickenFromCenterWhenRepelling;
                    chickenT.position = clipToXZ(transform.position) + randomOffset; //random position outside of tornado
                    chickenT.GetComponent<Rigidbody>().velocity = (clipToXZ(chickenT.position) - 
                                                                   clipToXZ(transform.position)).normalized
                        * repellingInitialSpeed;
                }

                // empty captureChickens array
                capturedChickens.Clear();
                
                // start countdown. at the end of this, become attractive again
                switchBackToAttractingCountdown.Restart();
            }
        }
    }
}
