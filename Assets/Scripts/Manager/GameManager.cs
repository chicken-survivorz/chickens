using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEngine.UI;
using PathCreation;
using TMPro;
using Unity.VisualScripting;
using System.Text.RegularExpressions;

public class GameManager : MonoBehaviour
{
    public const int MAX_FPS = 60;

    [SerializeField] Transform chickenGameObjectFolder;
    [SerializeField] int minChickenAliveBeforeGameOver = 20;
    [SerializeField] public UIManager UIManager;
    [SerializeField] PathCreator pathCreator;
    [SerializeField] TextMeshProUGUI fpsText;
    [SerializeField] TextMeshProUGUI levelText;
    [SerializeField] TextMeshProUGUI maxScoreText;
    [SerializeField] TextMeshProUGUI winText;
    [SerializeField] private float secondsToWaitBeforWinScreenAppears = 2.3f;



    [SerializeField] Canvas UICanvas;
    [SerializeField] Canvas winCanvas;
    [SerializeField] Canvas loseCanvas;

    public List<GameObject> galline; 

    public static GameManager instance;

    private bool endedWithVictory = false;
    void Awake()
    {
        Time.timeScale = 1;
        instance = this;
    }

    private void Start()
    {
        Application.targetFrameRate = Screen.currentResolution.refreshRate < MAX_FPS ? Screen.currentResolution.refreshRate : MAX_FPS;

        if (chickenGameObjectFolder.childCount < minChickenAliveBeforeGameOver)
        {
            Debug.LogError("inizi il livello con troppe poche galline");
            SceneManager.LoadScene("Assets/Scenes/MainLevel.unity");
        }

        for(int i = 0; i< chickenGameObjectFolder.childCount; i++) {

            GameObject gameObject = chickenGameObjectFolder.GetChild(i).gameObject;
            if (gameObject.CompareTag("Gallina"))
            {
                galline.Add(gameObject);
            }
        }

        GetComponent<ChickenSoundsManager>().MaxChickenNum = galline.Count;

        //UIManager.SetHealthSliderValues(minChickenAliveBeforeGameOver - 1, galline.Count, galline.Count);
        UIManager.SetProgressSliderValues(0, pathCreator.path.length, 0);
        UIManager.SetChickenCountContainer(galline.Count- minChickenAliveBeforeGameOver);

        levelText.text = "Level " + Regex.Match(SceneManager.GetActiveScene().name, @"\d+");
        maxScoreText.text = "Max Score: " + PlayerPrefs.GetInt(SceneManager.GetActiveScene().name + "MaxScore", 0);
    }

    private void Update()
    {
        if(fpsText!= null)
        fpsText.text = ((int)(1 / Time.deltaTime)).ToString();
    }

    public void OnGallinaDeath(GameObject gallinaCheMuore)
    {
        galline.Remove(gallinaCheMuore);

        if(galline.Count < minChickenAliveBeforeGameOver && !endedWithVictory) {
            var combine = FindObjectOfType<MakeGallineToBeInFuga>();
            if (combine != null)
            {
                combine.StopMoving();
                var spinnyPart = combine.GetComponentInChildren<SpinScript>();
                if (spinnyPart != null) spinnyPart.StopSpinning();
            }

            UICanvas?.gameObject.SetActive(false);
            loseCanvas?.gameObject.SetActive(true);

        }

        //UIManager.DecreseHealth();
        UIManager.RemoveAChickenFromCountContainer();
    }

    public void EndLevelWithVictory()
    {
        endedWithVictory = true;

        SetLevelProgress();

        if (galline.Count > PlayerPrefs.GetInt(SceneManager.GetActiveScene().name+"MaxScore", 0))
            PlayerPrefs.SetInt(SceneManager.GetActiveScene().name + "MaxScore", galline.Count);

        winText.text = "Your Score: " + galline.Count + "\nBest Score: " + PlayerPrefs.GetInt(SceneManager.GetActiveScene().name + "MaxScore", 0);

        var combine = FindObjectOfType<MakeGallineToBeInFuga>();
        if (combine != null)
        {
            combine.StopMoving();
            var spinnyPart = combine.GetComponentInChildren<SpinScript>();
            if (spinnyPart != null) spinnyPart.StopSpinning();
        }

        var coopParent = FindObjectOfType<EndLevel>();
        if (coopParent != null)
        {
            var victoryCoops = coopParent.GetComponentsInChildren<ChickenCoopSaver>();
            int coopCount = victoryCoops.Length;
            if (coopCount > 0)
            {
                List<GameObject> winners = galline;
                int winnersPerCoop = winners.Count / coopCount;
                for (; coopCount > 0; --coopCount)
                {
                    for (int i = (coopCount == 1 ? winners.Count : winnersPerCoop); i > 0 ; --i)
                    {
                        var chicken = winners[0].GetComponent<Chicken>();
                        chicken.AssignedCoop = victoryCoops[coopCount - 1];
                        chicken.state = ChickenMovementState.VICTORY_SPRINT;
                        winners[0].layer = LayerMask.NameToLayer("VicotryChicken");
                        winners.RemoveAt(0);
                    }
                }
            }
        }

        var cameraFollow = FindObjectOfType<FollowMainPericolo>();
        if (cameraFollow != null)
        {
            cameraFollow.StartVictoryPan();
        }
        StartCoroutine(WaitCoroutine());    
    }

    IEnumerator WaitCoroutine()
    {
        yield return new WaitForSeconds(secondsToWaitBeforWinScreenAppears);


        UICanvas.gameObject.SetActive(false);
        winCanvas.gameObject.SetActive(true);
    }

    public void ExitFromUI()
    {
        SceneManager.LoadScene("Assets/Scenes/MainLevel.unity");
    }

    public void RestartLevel() //Restarts the level
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void GoToTheNextLevel(int nextLevel)
    {
        SceneManager.LoadScene("Level"+nextLevel);
    }

    public void PauseResumeLevel()
    {
        if(Time.timeScale > 0)
            Time.timeScale = 0;
        else
            Time.timeScale = 1;
    }

    public void SetAudioActive(bool isActive)
    {
        if(isActive)
            AudioListener.volume = 1.0F;
        else
            AudioListener.volume = 0.0f;
    }

    private void SetLevelProgress()
    {
        string sceneName = SceneManager.GetActiveScene().name;
        sceneName = sceneName.Substring(sceneName.LastIndexOf("l", StringComparison.Ordinal)+1);
        if (int.TryParse(sceneName, out var levelValue))
        {
            if (levelValue > PlayerPrefs.GetInt("LevelProgress", 0))
                PlayerPrefs.SetInt("LevelProgress", levelValue);
        }
    }

}
