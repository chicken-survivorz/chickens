using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputManager : MonoBehaviour
{
    [HideInInspector]
    public static InputManager instance;
    public Collider ground;
    public float touchRadius = 5f;

    private Camera mainCamera;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        mainCamera = Camera.main;
    }

    /*Vector2? GetTouchNearPosition(Vector2 screenPos)
    {
#if UNITY_EDITOR
        if (Input.GetKey(KeyCode.Mouse0) && !EventSystem.current.IsPointerOverGameObject())
            return Input.mousePosition;
        return null;
#endif
        Vector2 nearestTouch;
        float nearestTouchDistance;
        if(Input.touchCount > 0)
        {
            nearestTouch = Input.GetTouch(0).position;
            nearestTouchDistance = Vector2.Distance(screenPos, nearestTouch);
            if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
            {
                return null;
            }
            for (int i = 1; i < Input.touchCount; i++)
            {

                if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(i).fingerId))
                {
                    continue;
                }

                float currentDistance = Vector2.Distance(screenPos, Input.GetTouch(i).position);
                if (currentDistance < nearestTouchDistance)
                {
                    nearestTouchDistance = currentDistance;
                    nearestTouch = Input.GetTouch(i).position;
                }
            }
            return nearestTouch;
        }
        return null;
    }*/

    Vector3? RaycastScreenPosToPlane(Vector2 screenPos)
    {
        Ray ray = mainCamera.ScreenPointToRay(screenPos);

        RaycastHit hit;
        if (ground.Raycast(ray, out hit, Mathf.Infinity))
        {
            //Debug.Log("Hit ground at " + hit.point);
            return hit.point;
        }
        return null;
    }

/*    public Vector3? GetNearestTouch(Vector3 position)
    {
        Vector2? touchNearPos = GetTouchNearPosition(mainCamera.WorldToScreenPoint(position)); ;
        if (touchNearPos != null)
        {
            Vector3? hitPos = RaycastScreenPosToPlane((Vector2)touchNearPos);
            return hitPos;
        }
        return null;
    }*/

    public Vector3? GetLatestTouch()
    {
        Vector2? nearestTouch = null;
#if UNITY_EDITOR
        if (Input.GetKey(KeyCode.Mouse0) && !EventSystem.current.IsPointerOverGameObject() )
            nearestTouch = Input.mousePosition;
#else
        if (Input.touchCount > 0)
            nearestTouch = Input.GetTouch(0).position;
#endif
        if (nearestTouch == null) return null;
        return RaycastScreenPosToPlane((Vector2)nearestTouch);
    }

    public List<Vector3> GetTouchPositions()
    {
        List<Vector3> touches = new List<Vector3>();
#if UNITY_EDITOR
        if (Input.GetKey(KeyCode.Mouse0) && !EventSystem.current.IsPointerOverGameObject())
        {
            Vector3? current = RaycastScreenPosToPlane(Input.mousePosition);
            touches.Add((Vector3)current);
        }
#else
        for (int i = 0; i < Input.touchCount; i++)
        {
            Vector3? current = RaycastScreenPosToPlane(Input.GetTouch(i).position);
            if(current != null)
                touches.Add((Vector3) current);
        }
#endif
        return touches;
    }
}
