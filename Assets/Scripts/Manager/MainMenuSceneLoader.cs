using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuSceneLoader : MonoBehaviour
{
    [SerializeField] private bool debugMode = false;
    private static int clearedLevels = 0;

    public void ChangeScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public static bool IsLevelEnabled(int level)
    {
        return level <= clearedLevels + 1;
    }

    private void Awake()
    {
        clearedLevels = PlayerPrefs.GetInt("LevelProgress", 0);
        if (debugMode) clearedLevels = 20;
    }
}
