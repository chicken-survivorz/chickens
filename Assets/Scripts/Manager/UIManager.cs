using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] Slider progressSlider;
    [SerializeField] GameObject chickenCountContainer;
    [SerializeField] GameObject chickenImage;

    private int chickenImageToDelete;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    //public void DecreseHealth()
    //{
    //    healthSlider.value--;
    //}

    //public void SetHealthSliderValues(float minValue, float maxValue, float value)
    //{
    //    SetSliderValues(healthSlider, minValue, maxValue, value);
    //}

    public void SetProgressSliderValues(float minValue, float maxValue, float value)
    {
        SetSliderValues(progressSlider, minValue, maxValue, value);
    }

    public void SetChickenCountContainer(int numberOfChickens)
    {
        chickenImageToDelete = numberOfChickens - 1;
        for(int i = 0; i < numberOfChickens; i++)
            Instantiate(chickenImage, chickenCountContainer.transform);
    }

    void SetSliderValues(Slider slider, float minValue, float maxValue, float value)
    {
        slider.minValue = minValue;
        slider.maxValue = maxValue;
        slider.value = value;
    }

    public void SetProgressValue(float value)
    {
        progressSlider.value = value;
    }

    public void RemoveAChickenFromCountContainer()
    {
        if(chickenCountContainer != null && chickenImageToDelete >= 0 &&chickenCountContainer.transform.GetChild(chickenImageToDelete))
        {
            Destroy(chickenCountContainer.transform.GetChild(chickenImageToDelete).gameObject);
            chickenImageToDelete--;
        }
    }
}
