using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLevel : MonoBehaviour
{
    public GameObject harvester;

#if UNITY_EDITOR
    private void Start()
    {
        Debug.Assert(harvester, "Assign harvester GameObject reference to EndLevel object!");
    }
#endif

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Harvester"))
        {
            GameManager.instance.EndLevelWithVictory();
        }
    }
}
