using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public static EnemyManager instance;
    public GameObject hungerParticleSystem;
    [HideInInspector] public List<Enemy> enemies;
    [HideInInspector] public List<Collider> hungerAreas;
    private Enemy dummyDanger1;
    private Enemy dummyDanger2;

    public float touchDistanceThreshold = 40f;
    private bool lastTouchWasInHungerZone = false;
    private ParticleSystem feedParticles;
    private readonly Vector3 farAway = new Vector3(100000000f, 0f, 1000000000f);

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        GameObject dummyEnemyGO = new GameObject("Dummy Enemy");
        dummyDanger1 = dummyEnemyGO.AddComponent<Enemy>();
        dummyDanger1.danger = 100f;
        dummyDanger1.IsSightOverriden = true;
        dummyDanger1.SightOverridedForThisEnemy = 40f;

        dummyDanger2 = Instantiate(dummyEnemyGO, farAway, Quaternion.identity).GetComponent<Enemy>();
       
        GameObject particleObject = Instantiate(hungerParticleSystem);
        feedParticles = particleObject.GetComponent<ParticleSystem>();
        feedParticles.Play();
        ParticleSystem.EmissionModule em = feedParticles.emission;
        em.enabled = false;

        foreach (var terrainEffect in FindObjectsOfType<TerrainEffect>().Where(t => t.statusTag == StatusEffectTag.Hungry))
        {
            hungerAreas.AddRange(terrainEffect.GetComponentsInChildren<Collider>());
        }
    }

    void Update()
    {
        lastTouchWasInHungerZone = false;
        Vector3? touchPosNullable = InputManager.instance.GetLatestTouch();
        if (touchPosNullable != null)
        {
            //add/move dummy enemy
            Vector3 touchPos = (Vector3)touchPosNullable;

            List<Vector3> touches = InputManager.instance.GetTouchPositions();
            for (int i = 0; i < 2; i++)
            {
                if (i < touches.Count)
                {
                    if (i == 0)
                        dummyDanger1.transform.position = touches[0];
                    else if (i == 1)
                        dummyDanger2.transform.position = touches[1];
                }
                else
                {
                    if (i == 0)
                        dummyDanger1.transform.position = farAway;
                    else if (i == 1)
                        dummyDanger2.transform.position = farAway;
                }
            }



            foreach (var hungerArea in hungerAreas)
            {
                touchPos.y = hungerArea.transform.position.y + 5;
                RaycastHit hit;

                if (hungerArea.Raycast(new Ray(touchPos, Vector3.down), out hit, 15f))
                {
                    touchPos.y = hungerArea.transform.position.y;
                    lastTouchWasInHungerZone = true;
                    break;
                }
            }

            if (lastTouchWasInHungerZone)
            {
                feedParticles.transform.position = touchPos;
                ParticleSystem.EmissionModule em = feedParticles.emission;
                em.enabled = true;
            }
            else
            {
                ParticleSystem.EmissionModule em = feedParticles.emission;
                em.enabled = false;
            }
        }
        else
        {
            //remove dummy enemy
            dummyDanger1.transform.position = farAway;
            dummyDanger2.transform.position = farAway;
            ParticleSystem.EmissionModule em = feedParticles.emission;
            em.enabled = false;
        }
    }

    public Vector3 GetNewDirection(Vector3 chickenPosition, float chickenViewRange, bool chickenIsPoisoned)
    {
        Vector3 newDirection = Vector3.zero;
        foreach (Enemy enemy in enemies)
        {
            if (chickenIsPoisoned && enemy.GetComponent<Wolf>() != null) continue;

            Collider collider = enemy.GetComponent<Collider>();
            float toCheck;
            float range = enemy.IsSightOverriden? enemy.SightOverridedForThisEnemy: chickenViewRange;

            Vector3 closestPoint = Vector3.zero;
            if (collider != null)
            {

                closestPoint = collider.ClosestPoint(chickenPosition);
                closestPoint.y = chickenPosition.y;
                toCheck = Vector3.Distance(chickenPosition, closestPoint);
            }
            else
                toCheck = Vector3.Distance(chickenPosition, enemy.transform.position);

            if (toCheck < range)
            {                
                Vector3 diff = Vector3.zero;
                if (collider != null && toCheck != 0)
                   diff = chickenPosition - closestPoint;
                else
                {
                   diff = chickenPosition - enemy.transform.position;
                }

                if ((enemy == dummyDanger1 || enemy == dummyDanger2) && lastTouchWasInHungerZone)
                    diff = -diff;

                newDirection += diff / diff.sqrMagnitude * enemy.danger;
            }
        }

        return newDirection.normalized;
    }

 

    public bool IsEnemyInRange(Vector3 chickenPosition, float chickenViewRange)
    {
        foreach( Enemy enemy in enemies)
        {
            var collider = enemy.GetComponent<Collider>();
            var rangeToCheck = enemy.IsSightOverriden ? enemy.SightOverridedForThisEnemy : chickenViewRange;
            if (collider == null)
            {
                if (Vector3.Distance(chickenPosition, enemy.transform.position) < rangeToCheck)
                {
                    return true;
                }
            }
            else
            {
                if (Vector3.Distance(chickenPosition, collider.ClosestPoint(chickenPosition)) < rangeToCheck)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
