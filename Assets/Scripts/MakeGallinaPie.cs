using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeGallinaPie : MonoBehaviour
{

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Gallina")) { 
            other.gameObject.GetComponent<Chicken>().Die();
        }
    }
}
