using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public class PathLines : MonoBehaviour
{
    public float sideOffset = 2;
    public PathCreator pathCreator;

#if UNITY_EDITOR

    private void OnDrawGizmos()
    {
        float dst = 0;
        VertexPath path = pathCreator.path;
        for (int i = 0; i < path.NumPoints; i++)
        {
            int nextI = i + 1;
            if (nextI >= path.NumPoints)
            {
                if (path.isClosedLoop)
                {
                    nextI %= path.NumPoints;
                }
                else
                {
                    break;
                }
            }

            Quaternion q = path.GetRotationAtDistance(dst);
            Gizmos.DrawLine(path.GetPoint(i) + q * Vector3.right * sideOffset, path.GetPoint(nextI) + q * Vector3.right * sideOffset);
            Gizmos.DrawLine(path.GetPoint(i) + q * Vector3.left * sideOffset, path.GetPoint(nextI) + q * Vector3.left * sideOffset);
            dst += Vector3.Distance(path.GetPoint(i), path.GetPoint(nextI));
        }
    }

#endif
}
