using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Unity.VisualScripting.Dependencies.NCalc;
using UnityEngine;
using UnityEngine.AI;

public enum WolfState
{
    DEAD,
    IDLE,
    CHASING,
    ATTACKING
}

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Animator))]
public class Wolf : Enemy
{
    public float retargetPeriod = 3f;
    public Transform chickensRoot;
    public float sightRange = 10f;
    public float searchForTargetFrequency = 0.5f; // just to make calculations lighter
    public float runningSpeed = 0.5f;
    public float wanderingSpeed = 0.2f;
    public float seekRotationCoefficient = 0.5f;
    public float changeDirWanderPeriod = 2f;
    // public float maxTurningSpeedDegPerSec = 360f;
    public float poisonAvoidanceCoefficient = 1f;

    private Timer retargetTimer;
    private Timer searchForTargetTimer;
    private Timer changeDirectionWhenWanderingTimer;
    [SerializeField] private Transform target;
    private Rigidbody rb;
    private Animator anim;

    public WolfState state;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
    }

    void Start()
    {
        AddSelfToManager();
        state = WolfState.IDLE;
        anim.SetTrigger("idle");
        
        retargetTimer = new Timer(retargetPeriod);
        AcquireTargetAndRestartTimer();

        searchForTargetTimer = new Timer(1f / searchForTargetFrequency);
        searchForTargetTimer.Start();

        changeDirectionWhenWanderingTimer = new Timer(changeDirWanderPeriod);
        changeDirectionWhenWanderingTimer.Start();
        
        // // animation
        // anim.SetTrigger("idle");
    }

    // Update is called once per frame
    void Update()
    {
        if (state != WolfState.DEAD && state != WolfState.ATTACKING)
        {
            // searching for target
            if (searchForTargetTimer.HasExpired() && // don't search every update, but every X seconds
                (
                    target != null && ! target.gameObject.activeSelf // if the target has turned inactive, it means it has entered a safe zone
                    || retargetTimer.HasExpired() // change target every once in a while
                    || target == null // if the target has become null (e.g. it has died), search for a new one
                )
            )
            {
                var acquired = AcquireTarget();
                if (state == WolfState.CHASING)
                {
                    retargetTimer.Restart();
                }

                searchForTargetTimer.Restart();
            }

            // pursuit target
            // if (target != null)
            if (state == WolfState.CHASING)
            {
                // anim.SetTrigger("run")
                Seek();
                Face();
                // transform.LookAt(transform.position + rb.velocity);
            }
            // if it has no target, wander in random directions every
            // X seconds
            else if (changeDirectionWhenWanderingTimer.HasExpired())
            {
                state = WolfState.IDLE;
                anim.SetTrigger("idle");

                Wander();
                changeDirectionWhenWanderingTimer.Restart();
                // transform.LookAt(transform.position + rb.velocity);
                Face();
            }

            // regardless, we want to avoid poisoned chickens
            AvoidPoisoned();
            Face();
        }
    }

    private void Face()
    {
        transform.LookAt(transform.position + rb.velocity);
        
        // var a = Vector3.Angle(transform.forward, rb.velocity.normalized);
        // var b = maxTurningSpeedDegPerSec * Time.deltaTime;
        // transform.Rotate(
        //     0, 
        //     Mathf.Min(
        //         a,
        //         b
        //     ),
        //     0);
    }

    private void OnCollisionEnter(Collision other)
    {
        var g = other.gameObject.GetComponent<Chicken>();
        if (g)
        {
            // if the chicken is poisoned, YOU die
            if(g.Status?.GetStatusTag() == StatusEffectTag.Poison)
            {
                state = WolfState.DEAD;
                anim.SetTrigger("die");
                GetComponent<Collider>().enabled = false;
                rb.velocity = Vector3.zero;
                rb.freezeRotation = true;
                EnemyManager.instance.enemies.Remove(this);
                Destroy(gameObject, 3f);
                this.enabled = false;
            }
            else if (state != WolfState.ATTACKING)
            {
                // we use this state as a flag so that the wolf can't kill other
                // chickens on contact while he's performing his attack
                StartCoroutine(Attack(g));
            }
        }
    }

    private IEnumerator Attack(Chicken prey)
    {
        state = WolfState.ATTACKING;
        
        // attack animation
        anim.SetTrigger("attack");

        // block chicken and kill it after animation is completed
        prey.state = ChickenMovementState.PERMA_STUNNED;
        yield return new WaitForSeconds(1f);
        prey.Die();
        target = null;

        state = WolfState.IDLE;
    }

    private bool AcquireTargetAndRestartTimer()
    {
        var acquired = AcquireTarget();
        retargetTimer.Restart();
        return acquired;
    }

    private bool AcquireTarget()
    {
        (var closestChicken, var minDistance) = GetClosestNonPoisonedChickenAndDistance();
        if (closestChicken != null && minDistance < sightRange)
        {
            target = closestChicken;
            
            // update state
            state = WolfState.CHASING;
            // change animator
            anim.SetTrigger("run");
            
            return true;
        }
        
        // Nothing to chase
        return false;
    }

    private (Transform, float) GetClosestNonPoisonedChickenAndDistance()
    {
        var minD = Mathf.Infinity;
        Transform closestChicken = null;
        foreach (Transform chickenT in chickensRoot)
        {
            if (!chickenT.gameObject.activeSelf)
                continue;
            var d = Vector3.Distance(chickenT.position, this.transform.position);
            if (d < minD
                && chickenT.GetComponent<Chicken>().Status?.GetStatusTag() != StatusEffectTag.Poison) // don't consider poisoned chickens
            {
                minD = d;
                closestChicken = chickenT;
            }
        }

        return (closestChicken, minD);
    }

    private void Seek()
    {
        if (target != null)
        {
            // we normalize it first since it could be set at the wandering speed
            // and that would affect the sum below
            rb.velocity = rb.velocity.normalized;

            // deviate
            rb.velocity += seekRotationCoefficient * (target.position - this.transform.position).normalized;

            // renormalize
            rb.velocity = rb.velocity.normalized * runningSpeed;
        }
    }

    private void AvoidPoisoned()
    {
        // compute correction to avoid poisonous chickens
        Vector3 speedCorrection = Vector3.zero;
        foreach (Transform chickenT in chickensRoot)
        {
            var g = chickenT.GetComponent<Chicken>();
            if (g.Status != null && g.Status.GetStatusTag() == StatusEffectTag.Poison)
            {
                var d = transform.position - chickenT.transform.position;
                if (d.sqrMagnitude > 0)
                {
                    speedCorrection += d / d.sqrMagnitude * poisonAvoidanceCoefficient; // close chickens weigh more
                }
            }
        }
        
        // modify velocity
        rb.velocity += speedCorrection;
        
        // renormalize
        rb.velocity = rb.velocity.normalized * runningSpeed;
    }

    private void Wander()
    {
        // take a random direction at speed = wanderingSpeed
        rb.velocity = (
            Quaternion.Euler(0, UnityEngine.Random.Range(0, 360f), 0)
            * Vector3.forward
        ).normalized * wanderingSpeed;
    }
}