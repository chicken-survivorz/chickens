using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FenceBuilder : MonoBehaviour
{
    public GameObject[] fences;
    public GameObject[] gates;
    public GameObject[] barns;
    public BoxCollider batchCollider;
    public float gateChance;
    public float barnChance;
    public float rollAngle;
    public int length;
    private Vector3 colliderOffset;

    [Header("SpawnDirection")]
    public Vector3 direction;
    public float yawAngle;
    public bool isLeft;

    float prevLength = 0;

    void Start()
    {
        if (isLeft) rollAngle = -rollAngle;
        colliderOffset = batchCollider.center;
        BatchSpawn();
    }

    void BatchSpawn()
    {
        GameObject spawnedPiece;
        Collider collider;
        for (int i = 0; i < length; i++)
        {
            bool isBarn = false;
            float choice = Random.Range(0f, 100f);
            if (choice < gateChance)
            {
                int index = Random.Range(0, gates.Length);
                spawnedPiece = Instantiate(gates[index], transform.position + prevLength * direction, Quaternion.identity);
            }
            else if (choice < gateChance + barnChance)
            {
                isBarn = true;
                int index = Random.Range(0, barns.Length);
                spawnedPiece = Instantiate(barns[index], transform.position + prevLength * direction, Quaternion.identity);
            }
            else
            {
                int index = Random.Range(0, fences.Length);
                spawnedPiece = Instantiate(fences[index], transform.position + prevLength * direction, Quaternion.identity);
            }
            collider = spawnedPiece.GetComponentInChildren<Collider>();
            if (collider != null) prevLength += collider.bounds.size.x;
            spawnedPiece.transform.rotation = Quaternion.Euler(isBarn ? 0f : rollAngle, yawAngle, 0f);
            if (!isLeft) spawnedPiece.transform.localScale = new Vector3(1, 1, -1);
        }
        batchCollider.center = colliderOffset + prevLength * direction;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("MainCamera"))
        {
            BatchSpawn();
        }
    }
}
