using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleAdjuster : MonoBehaviour
{
    private Toggle toggle;

    private void Awake()
    {
        toggle = GetComponent<Toggle>();
    }

    void Start()
    {
        if(toggle != null)
        {
            if(AudioListener.volume != 0)
                toggle.isOn = true;
            else
                toggle.isOn = false;
        }
    }
}
