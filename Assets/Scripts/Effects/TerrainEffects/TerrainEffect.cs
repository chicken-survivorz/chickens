using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainEffect : MonoBehaviour
{
    public StatusEffectTag statusTag;
    public Material statusMaterial; //temporaneo per debug, magari sostituire con particle effect
    public GameObject statusParticle;

    protected void OnTriggerEnter(Collider other)
    {
        Chicken g;
        if (g = other.GetComponent<Chicken>())
        {
            g.RemoveParticles();
            switch(statusTag)
            {
                case StatusEffectTag.Water:
                    g.Status = new WaterStatus(g, statusMaterial, statusParticle);
                    break;
                case StatusEffectTag.Fire:
                    g.Status = new FireStatus(g, statusMaterial, statusParticle);
                    break;
                case StatusEffectTag.Electrified:
                    if(g.Status?.GetStatusTag() == StatusEffectTag.Water)
                    {
                        g.Status = new ElectricStatus(g, statusMaterial);
                    }
                    break;
                case StatusEffectTag.Hungry:
                    break;
                case StatusEffectTag.Poison:
                    g.Status = new PoisonStatus(g, statusMaterial, statusParticle);
                    break;
                case StatusEffectTag.Muddy:
                    g.Status = new MuddyStatus(g, statusMaterial, statusParticle);
                    break;
                case StatusEffectTag.Stunned:
                    g.Status = new StunnedStatus(g);
                    Animator animator = GetComponent<Animator>();
                    if(animator && animator.GetCurrentAnimatorStateInfo(0).IsName("RakeIdle"))
                    {
                        animator.SetTrigger("stun");
                    }
                    break;
            }
            g.Status?.EnterEffectArea();
        }
    }

    protected void OnTriggerExit(Collider other)
    {
        Chicken g;
        
        if (g = other.GetComponent<Chicken>())
        {
            if (statusTag == StatusEffectTag.Hungry)
            {
                return;
            }
            g.Status?.ExitEffectArea();
        }
    }
}
