using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterTerrainEffect : TerrainEffect
{
    public Material electrifiedMaterial;

    private uint electrifiedObjects = 0;
    private Material waterMaterial;

    private new void OnTriggerEnter(Collider other)
    {
        TerrainEffect tf;
        if((tf = other.GetComponent<TerrainEffect>()) && tf.statusTag == StatusEffectTag.Electrified)
        {
            if(electrifiedObjects == 0)
            {
                //TODO: thunder electric particles? to make it clear the water is electrified
                statusTag = StatusEffectTag.Electrified;
                
                if(waterMaterial == null)
                    waterMaterial = statusMaterial;
                statusMaterial = electrifiedMaterial;
            }
            electrifiedObjects++;
        }

        //if the chicken is dry, assign water so in base.OnTriggerEnter will get electrified
        Chicken g = other.GetComponent<Chicken>();
        if(g)
            g.Status = new WaterStatus(g, statusMaterial, statusParticle);

        base.OnTriggerEnter(other);
    }

    private void OnTriggerStay(Collider other)
    {
        Chicken g = other.GetComponent<Chicken>();
        if (g && g.Status == null)
            g.Status = new WaterStatus(g, statusMaterial, statusParticle);
    }

    private new void OnTriggerExit(Collider other)
    {
        TerrainEffect tf;
        if ((tf = other.GetComponent<TerrainEffect>()) && tf.statusTag == StatusEffectTag.Electrified)
        {
            electrifiedObjects--;
            if (electrifiedObjects == 0)
            {
                statusTag = StatusEffectTag.Water;
                statusMaterial = waterMaterial;
            }
        }

        base.OnTriggerExit(other);
    }

    private bool IsElectrified()
    {
        return electrifiedObjects > 0;
    }
}
