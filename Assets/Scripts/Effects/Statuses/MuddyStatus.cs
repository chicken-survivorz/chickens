using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MuddyStatus : IStatus
{
    public static readonly float duration = 5f;
    public static readonly float slowingSpeedMultiplier = .5f;
    
    Material mat;
    Chicken chicken;
    Timer timer = new Timer(duration);
    GameObject particles;

    public MuddyStatus(Chicken g, Material m, GameObject particles)
    {
        chicken = g;
        mat = m;
        this.particles = particles;
        chicken.SetParticleStatus(particles);
    }

    public void EnterEffectArea()
    {
        chicken.smRenderer.material = mat;
        
        // slowdown effect
        chicken.speedMultiplier = slowingSpeedMultiplier;
    }

    public void Update()
    {
        if (timer.HasExpired())
        {
            chicken.smRenderer.material = chicken.idleMat;
            chicken.Status = null;
            chicken.SetParticleStatus(null);
            
            // back to normal speed
            chicken.speedMultiplier = 1f;
        }
    }

    public StatusEffectTag GetStatusTag()
    {
        return StatusEffectTag.Muddy;
    }

    public void ExitEffectArea()
    {
        timer.Reset();
        timer.Start();
    }
}
