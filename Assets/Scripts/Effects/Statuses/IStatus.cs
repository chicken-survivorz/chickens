using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StatusEffectTag
{
    Water,
    Fire,
    Electrified,
    Hungry,
    Poison,
    Stunned,
    Muddy
}

public interface IStatus
{
    public void EnterEffectArea();
    public void Update();
    public void ExitEffectArea();
    public StatusEffectTag GetStatusTag();
}

public interface IStatusWithPropagation : IStatus
{
    public void CollisionEnter(Collision collision);
    public void CollisionStay(Collision collision);
    public void CollisionExit(Collision collision);
}
