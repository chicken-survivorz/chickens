using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricStatus : IStatus
{
    private readonly static int electrifiedTime = 1;

    Material material;
    Chicken chicken;

    public ElectricStatus(Chicken g, Material m)
    {
        chicken = g;
        material = m;
        chicken.smRenderer.material = material;
    }

    public void EnterEffectArea()
    {
        //block the chicken
        //TODO: make animation
        //chicken die
        chicken.state = ChickenMovementState.PERMA_STUNNED;
        chicken.Die(electrifiedTime);
    }

    public void ExitEffectArea()
    {
    }

    public StatusEffectTag GetStatusTag()
    {
        return StatusEffectTag.Electrified;
    }

    public void Update()
    {
    }
}
