using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunnedStatus : IStatus
{
    Chicken chicken;
    Timer timer = new Timer(2);

    public StunnedStatus(Chicken g)
    {
        chicken = g;
        chicken.state = ChickenMovementState.STUNNED;
        timer.Start();
        g.stunnedParticles.SetActive(true);
    }

    public void EnterEffectArea()
    {
    }

    public void ExitEffectArea()
    {
    }

    public StatusEffectTag GetStatusTag()
    {
        return StatusEffectTag.Stunned;
    }

    void IStatus.Update()
    {
        if (timer.HasExpired())
        {
            chicken.state = ChickenMovementState.IDLE;
            chicken.Status = null;
            chicken.stunnedParticles.SetActive(false);
        }
    }
}
