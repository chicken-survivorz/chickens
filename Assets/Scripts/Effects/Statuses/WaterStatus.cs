using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WaterStatus : IStatus
{
    /* wetMaterial solo per debug per vedere
     * quali galline sono bagnate.
     * Si può pensare ad un particellare delle gocce o qualcosa del genere
     */
    Material wetMaterial;
    Chicken chicken;
    public static readonly int duration = 3;
    Timer timer = new Timer(duration);
    GameObject particles;

    public WaterStatus(Chicken g, Material m, GameObject particles)
    {
        chicken = g;
        wetMaterial = m;
        this.particles = particles;
        chicken.SetParticleStatus(particles);
        //chicken.RemoveParticles();
    }

    public void EnterEffectArea()
    {
        //applica effetto bagnato alla gallina
        chicken.smRenderer.material = wetMaterial;
    }

    public void Update()
    {
        if (timer.HasExpired())
        {
            //rimuovi effetto grafico bagnato alla gallina
            chicken.smRenderer.material = chicken.idleMat;
            chicken.Status = null;
            chicken.SetParticleStatus(null);
        }
    }

    public StatusEffectTag GetStatusTag()
    {
        return StatusEffectTag.Water;
    }

    public void ExitEffectArea()
    {
        timer.Reset();
        timer.Start();
    }
}
