using System.Collections.Generic;
using UnityEngine;

public class FireStatus : IStatusWithPropagation
{
    static readonly float propagationTime = .7f; //time it takes to propagate the effect to another chicken when colliding with it
    
    Material material;
    Chicken chicken;
    Timer deathTimer = new Timer(5);
    Dictionary<GameObject, float> collisionDict = new Dictionary<GameObject, float>();
    GameObject particles;

    public FireStatus(Chicken g, Material m, GameObject particles)
    {
        chicken = g;
        material = m;
        chicken.smRenderer.material = material;
        this.particles = particles;
        chicken.SetParticleStatus(particles);

    }

    public void EnterEffectArea()
    {
        
    }

    public void Update()
    {

        if (!deathTimer.Started())
        {
            deathTimer.Start();
        }
        if (deathTimer.HasExpired())
        {
            //The chicken dies
            chicken.Die();
        }
    }

    public void ExitEffectArea()
    {}

    public StatusEffectTag GetStatusTag()
    {
        return StatusEffectTag.Fire;
    }

    public void CollisionEnter(Collision collision)
    {
        //Debug.Log("Trigger enter IStatusWithPropagation");
        if(collision.gameObject.tag == "Gallina")
        {
            if(!collisionDict.ContainsKey(collision.gameObject))
            {
                collisionDict[collision.gameObject] = Time.time;
            }
        }
    }

    public void CollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Gallina")
        {
            /* The chicken could have executed OnCollisionEnter before
             * getting this status, so we have to check again to make sure 
             * that the status propagates even if in this case
             * (i.e.: the chicken is blocked by other chicken and it stays still,
             * constantly colliding)
             */
            if(!collisionDict.ContainsKey(collision.gameObject))
            {
                collisionDict[collision.gameObject] = Time.time;
            }
            /* else if because we are sure (assuming propagationTime strictly > 0) that
             * the frame in which the effect should propagate is not the same as the one
             * in which the time is added to collisionDict
             */
            else if (Time.time - collisionDict[collision.gameObject] >= propagationTime)
            {
                Chicken otherChicken = collision.gameObject.GetComponent<Chicken>();

                if(otherChicken.Status?.GetStatusTag() != StatusEffectTag.Fire)
                    otherChicken.Status = new FireStatus(otherChicken, material, particles);
                else
                    collisionDict.Remove(collision.gameObject);
            }
        }
    }

    public void CollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Gallina")
        {
            collisionDict.Remove(collision.gameObject);
        }
    }
}
