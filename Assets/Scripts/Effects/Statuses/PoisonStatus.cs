using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoisonStatus : IStatus
{
    Material poisonMaterial;
    Chicken chicken;
    Timer timer = new Timer(10);
    GameObject particles;

    public PoisonStatus(Chicken g, Material m, GameObject particles)
    {
        chicken = g;
        poisonMaterial = m;
        chicken.smRenderer.material = poisonMaterial;
        chicken.SetParticleStatus(particles);
    }

    public void EnterEffectArea()
    {
        
    }

    public void ExitEffectArea()
    {
        
    }

    public StatusEffectTag GetStatusTag()
    {
        return StatusEffectTag.Poison;
    }

    // Start is called before the first frame update


    void IStatus.Update()
    {

        if (!timer.Started())
        {
            timer.Start();
        }
        if (timer.HasExpired())
        {
            chicken.Die();
        }
    }
}
