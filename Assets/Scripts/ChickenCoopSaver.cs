using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ChickenCoopSaver : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Use -1 for no limit")]
    private int MaxCapacity =-1;

    [SerializeField] private GameObject plusOneParticleSystem;
    [SerializeField] private GameObject hayParticleSystem;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private TMP_Text text;
    [SerializeField] private GameObject doors;

    private ParticleSystem haySystem;

    private int currentCapacity;

    private void Start()
    {
        currentCapacity = MaxCapacity;
        if (MaxCapacity == -1)
        {
            text.gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        if (currentCapacity != -1)
        {
            text.text = $"{MaxCapacity - currentCapacity} / {MaxCapacity}";
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Gallina"))
        {
            if (currentCapacity != 0)
            {
                other.gameObject.SetActive(false);
                if (currentCapacity > 0) --currentCapacity;
                if (currentCapacity == 0) doors.SetActive(true);

                Instantiate(plusOneParticleSystem, this.transform.position, Quaternion.identity, this.transform);
                audioSource.Play();
                if (!haySystem)
                    haySystem = Instantiate(hayParticleSystem, this.transform.position, this.transform.rotation, this.transform).GetComponent<ParticleSystem>();
                if (!haySystem.isPlaying)
                    haySystem.Play();
            }
        }
    }
}
