using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float danger;

    public bool IsSightOverriden = false;

    public float SightOverridedForThisEnemy = 20f;

    void Start()
    {
        AddSelfToManager();
    }

    public void AddSelfToManager()
    {
        EnemyManager.instance.enemies.Add(this);

    }
}