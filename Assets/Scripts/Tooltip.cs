using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Tooltip : MonoBehaviour
{
    public string tooltip = "Insert Tooltip Here";
    public int wordsPerMinute = 200; // average WPM reading speed

    private static readonly float avgWordLength = 4.7f;
    private bool wasShown = false;
    [SerializeField] private Canvas tooltipCanvas;
    [SerializeField] private TextMeshProUGUI text;
    
    private void OnTriggerEnter(Collider other)
    {
        if (!wasShown)
        {
            var g = other.gameObject.GetComponent<Chicken>();
            if (g)
            {
                wasShown = true;

                text.text = tooltip;
                
                tooltipCanvas.gameObject.SetActive(true);
                Invoke("DeactivateTooltip", 60.0f / wordsPerMinute * tooltip.Length / avgWordLength);
            }
        }
    }

    private void DeactivateTooltip()
    {
        tooltipCanvas.gameObject.SetActive(false);
        Destroy(this);
    }
    
}
