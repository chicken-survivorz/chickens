using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoleHole : MonoBehaviour
{
    [SerializeField] private GameObject otherHole;
    private SphereCollider sphereCollider;

    void Start()
    {
        sphereCollider = GetComponent<SphereCollider>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(otherHole && other.gameObject.CompareTag("Gallina") && Vector3.Distance(transform.position, other.transform.position) > sphereCollider.radius)
        {
            other.gameObject.transform.position = otherHole.transform.position;
        }
    }
}
