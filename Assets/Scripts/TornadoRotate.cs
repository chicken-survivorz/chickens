using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TornadoRotate : MonoBehaviour
{
    public GameObject model;
    public float angularSpeed = 5f;

    void Update()
    {
        model.transform.Rotate(Vector3.up, angularSpeed * Time.deltaTime);
    }
}
